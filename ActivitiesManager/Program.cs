﻿using System;
using System.Text;
using ConfigManager;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ActivitiesManager
{
    class Program
    {
        static void Main()
        {
            var factory = new ConnectionFactory
            {
                HostName = ConfigReader.RabbitMQ.HostName,
                UserName = ConfigReader.RabbitMQ.UserName,
                Password = ConfigReader.RabbitMQ.Password
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare("get_activities", false, false, false, null);
            channel.BasicQos(0, 1, false);

            var consumer = new EventingBasicConsumer(channel);

            channel.BasicConsume("get_activities", false, consumer);
            Console.WriteLine(" [x] Awaiting requests");

            consumer.Received += (model, ea) =>
            {
                var props = ea.BasicProperties;
                var replyProps = channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;
                //todo: здесь сериализуем и отправляем в виде строки список доступных активностей
                string response = "test!";
                var responseBytes = Encoding.UTF8.GetBytes(response);
                channel.BasicPublish("", props.ReplyTo, replyProps, responseBytes);
                Console.WriteLine(" [x] Sent message");
                channel.BasicAck(ea.DeliveryTag, false);
            };

            Console.ReadLine();
        }
    }
}
