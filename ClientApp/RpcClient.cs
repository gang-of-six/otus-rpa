﻿using System;
using System.Collections.Concurrent;
using System.Text;
using ConfigManager;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ClientApp
{
    class RpcClient
    {
        private readonly IConnection connection;
        private readonly IModel channel;
        private readonly string replyQueueName;
        private readonly EventingBasicConsumer consumer;
        private readonly BlockingCollection<string> respQueue = new BlockingCollection<string>();
        private readonly IBasicProperties props;

        public RpcClient()
        {
            var factory = new ConnectionFactory
            {
                HostName = ConfigReader.RabbitMQ.HostName,
                UserName = ConfigReader.RabbitMQ.UserName,
                Password = ConfigReader.RabbitMQ.Password
            };

            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ConfirmSelect();
            replyQueueName = channel.QueueDeclare().QueueName;
            consumer = new EventingBasicConsumer(channel);

            props = channel.CreateBasicProperties();
            var correlationId = Guid.NewGuid().ToString();
            props.CorrelationId = correlationId;
            props.ReplyTo = replyQueueName;

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var response = Encoding.UTF8.GetString(body);
                if (ea.BasicProperties.CorrelationId == correlationId)
                {
                    respQueue.Add(response);
                }
            };
        }

        public string Call()
        {
            channel.BasicPublish("", "get_activities", props, null);
            channel.BasicConsume(consumer, replyQueueName, true);
            channel.WaitForConfirmsOrDie(new TimeSpan(0, 0, 5));
            return respQueue.Take();
        }

        public void Close()
        {
            connection.Close();
        }
    }
}
