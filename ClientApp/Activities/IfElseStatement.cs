﻿using System;

namespace OtusRPA.Activities
{
    public class IfElseStatement : Activity
    {
        public override (bool, object) Do()
        {
            if (true)
            {
                return (true, null);
            }
            //else
            //{
            //    return (false, null);
            //}
        }

        public IfElseStatement()
        {
            InTypes.Add(Guid.NewGuid(), null);
            OutTypes.Add(Guid.NewGuid(), null);
            OutTypes.Add(Guid.NewGuid(), null);

            Name = "If Statement";
        }
    }
}
