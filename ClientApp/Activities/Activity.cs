﻿using System;
using System.Collections.Generic;

namespace OtusRPA.Activities
{
    public abstract class Activity
    {
        public Dictionary<Guid, Type> InTypes { get; set; }
        public Dictionary<Guid, Type> OutTypes { get; set; }
        public string Name { get; protected set; }
        public abstract (bool, object) Do();

        public Activity()
        {
            InTypes = new Dictionary<Guid, Type>();
            OutTypes = new Dictionary<Guid, Type>();
        }
    }
}
