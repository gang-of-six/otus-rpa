﻿using System;

namespace OtusRPA.Activities
{
    public class CreateFolder : Activity
    {
        public override (bool, object) Do()
        {
            return (true, null);
        }

        public CreateFolder()
        {
            InTypes.Add(Guid.NewGuid(), typeof(string));
            OutTypes.Add(Guid.NewGuid(), null);
        }
    }
}
