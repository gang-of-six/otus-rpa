﻿using System;

namespace OtusRPA.Activities
{
    public class End : Activity
    {
        public override (bool, object) Do()
        {
            return (true, null);
        }

        public End()
        {
            InTypes.Add(Guid.NewGuid(), typeof(bool));
        }
    }
}
