﻿using System;
using System.Windows;

namespace OtusRPA.Activities
{
    public class ShowMessage : Activity
    {
        public override (bool, object) Do()
        {
            MessageBox.Show("");
            return (true, null);
        }

        public ShowMessage()
        {
            InTypes.Add(Guid.NewGuid(), typeof(string));
            OutTypes.Add(Guid.NewGuid(), null);
        }
    }
}
