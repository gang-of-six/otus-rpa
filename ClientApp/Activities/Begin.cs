﻿using System;

namespace OtusRPA.Activities
{
    public class Begin : Activity
    {
        public override (bool, object) Do()
        {
            return (true, null);
        }

        public Begin()
        {
            OutTypes.Add(Guid.NewGuid(), null);
        }
    }
}
