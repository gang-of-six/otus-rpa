﻿using System.Collections.Generic;
using System.Windows;
using ClientApp;
using OtusRPA.Activities;
using OtusRPA.UI;

namespace OtusRPA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Rectangle> rectangles;

        public MainWindow()
        {
            InitializeComponent();
            rectangles = new List<Rectangle>();

            //todo: выводим loading screen с информацией о загрузке активностей
            //todo: сделать таймаут, после которого будем выбрасывать ошибку и закрывать приложение

            var rpcClient = new RpcClient();
            var response = rpcClient.Call();
            rpcClient.Close();

            MessageBox.Show(response);

            //todo: здесь десериализуем список активностей и добавляем в интерфейс

            rectangles.Add(new Rectangle(new Begin()));
            rectangles.Add(new Rectangle(new End()));
            rectangles.Add(new Rectangle(new ShowMessage()));
            rectangles.Add(new Rectangle(new CreateFolder()));
            rectangles.Add(new Rectangle(new IfElseStatement()));

            foreach (var item in rectangles)
            {
                listActivities.Children.Add(item);
            }
        }
    }
}
