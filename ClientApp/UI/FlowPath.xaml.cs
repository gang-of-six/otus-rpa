﻿using System.Windows;
using System.Windows.Controls;

namespace OtusRPA.UI
{
    /// <summary>
    /// Interaction logic for FlowPath.xaml
    /// </summary>
    public partial class FlowPath : UserControl
    {
        public Point StartPoint
        {
            get
            {
                return pFig.StartPoint;
            }
            set
            {
                pFig.StartPoint = value;
                pSegment.Point1 = new Point(StartPoint.X, StartPoint.Y + 70);
            }
        }

        public Point EndPoint
        {
            get
            {
                return pSegment.Point3;
            }
            set
            {
                pSegment.Point3 = value;
                pSegment.Point2 = new Point(EndPoint.X, EndPoint.Y - 70);
            }
        }

        public FlowPath()
        {
            InitializeComponent();
        }

        public FlowPath(Point startPoint, Point endPoint)
        {
            InitializeComponent();

            StartPoint = startPoint;
            EndPoint = endPoint;
        }
    }
}
