﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OtusRPA.UI
{
    /// <summary>
    /// Interaction logic for CanvasFlow.xaml
    /// </summary>
    public partial class CanvasFlow : UserControl
    {
        private Point dropPoint;
        private FlowPath path = null;
        private DataObject draggedData = null;

        public CanvasFlow()
        {
            InitializeComponent();
        }

        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            if (e.Data.GetDataPresent(typeof(IFlow)) &&
                path != null)
            {
                if (cnvUI.Children.Contains(path))
                    cnvUI.Children.Remove(path);
                draggedData = null;
            }

            if (e.Data.GetDataPresent(typeof(Rectangle)) &&
                e.Data.GetData(typeof(Rectangle)) is Rectangle rectangle &&
                WpfHelper.FindAncestorOrSelf<Panel>(rectangle) is Panel parent)
            {
                Point offset = rectangle.DragPoint.Value;
                double dropX = Math.Clamp(dropPoint.X - offset.X, 0, RenderSize.Width - rectangle.RenderSize.Width);
                double dropY = Math.Clamp(dropPoint.Y - offset.Y, 0, RenderSize.Height - rectangle.RenderSize.Height);

                if (parent.Name == "listActivities" && e.AllowedEffects.HasFlag(DragDropEffects.Copy) ||
                    parent == cnvUI && e.KeyStates == DragDropKeyStates.AltKey)
                {
                    Rectangle copyRectangle = new Rectangle(rectangle);
                    copyRectangle.SetValue(Canvas.LeftProperty, dropX);
                    copyRectangle.SetValue(Canvas.TopProperty, dropY);
                    cnvUI.Children.Add(copyRectangle);
                    e.Effects = DragDropEffects.Copy;
                }
                else if (parent == cnvUI && e.AllowedEffects.HasFlag(DragDropEffects.Move))
                {
                    rectangle.SetValue(Canvas.LeftProperty, dropX);
                    rectangle.SetValue(Canvas.TopProperty, dropY);
                    e.Effects = DragDropEffects.Move;
                }
            }
        }

        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);

            if (e.Data.GetDataPresent(typeof(Rectangle)) &&
                e.Data.GetData(typeof(Rectangle)) is Rectangle rectangle)
            {
                dropPoint = e.GetPosition(cnvUI);

                if (WpfHelper.FindAncestorOrSelf<Panel>(rectangle) is StackPanel || e.KeyStates == DragDropKeyStates.AltKey)
                {
                    e.Effects = DragDropEffects.Copy;
                }
                else if (WpfHelper.FindAncestorOrSelf<Panel>(rectangle) is Canvas)
                {
                    e.Effects = DragDropEffects.Move;
                }
            }
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            if (e.Data.GetDataPresent(typeof(IFlow)) &&
                e.Data.GetData(typeof(IFlow)) is IFlow srcFlow)
            {
                draggedData = (DataObject)e.Data;
                DrawPath(srcFlow, e.GetPosition(this));
            }
        }

        protected override void OnDragLeave(DragEventArgs e)
        {
            base.OnDragLeave(e);

            if (e.Data.GetDataPresent(typeof(IFlow)) &&
                e.Effects == DragDropEffects.Link)
            {
                if (cnvUI.Children.Contains(path))
                    cnvUI.Children.Remove(path);
                draggedData = null;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed && draggedData != null)
            {
                if (draggedData.GetDataPresent(typeof(IFlow)) &&
                    draggedData.GetData(typeof(IFlow)) is IFlow srcFlow)
                {
                    DrawPath(srcFlow, e.GetPosition(this));
                }
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            if (cnvUI.Children.Contains(path))
            {
                cnvUI.Children.Remove(path);
                path = null;
            }
        }

        private void DrawPath(IFlow srcFlow, Point curPoint)
        {
            var srcFlowControl = (UserControl)srcFlow;

            Point connectionPoint = srcFlow.AbsoluteConnectionPoint;

            if (srcFlow is FlowIn)
            {
                if (path != null && cnvUI.Children.Contains(path))
                {
                    path.StartPoint = curPoint;
                }
                else
                {
                    path = new FlowPath(curPoint, connectionPoint);
                    cnvUI.Children.Add(path);
                }
            }
            else
            {
                if (path != null && cnvUI.Children.Contains(path))
                {
                    path.EndPoint = curPoint;
                }
                else
                {
                    path = new FlowPath(connectionPoint, curPoint);
                    cnvUI.Children.Add(path);
                }
            }
        }
    }
}
