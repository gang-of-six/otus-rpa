﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OtusRPA.UI
{
    /// <summary>
    /// Interaction logic for FlowIn.xaml
    /// </summary>
    public partial class FlowIn : UserControl, IFlow
    {
        private Point relativeConnectionPoint;
        private Point absoluteConnectionPoint;
        private FlowPath flowPath;
        private bool busy;

        public Point AbsoluteConnectionPoint
        {
            get => absoluteConnectionPoint;
            set
            {
                absoluteConnectionPoint = value;
                if (busy)
                    flowPath.EndPoint = AbsoluteConnectionPoint;
            }
        }

        public FlowIn(int i)
        {
            InitializeComponent();

            relativeConnectionPoint = TranslatePoint(new Point(0, 0), Parent as UIElement);
            relativeConnectionPoint.Offset(Width / 2, 0);

            AbsoluteConnectionPoint = new Point(0, 0);

            busy = false;
            Name = GetType().Name + i;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.LeftButton == MouseButtonState.Pressed &&
                WpfHelper.FindAncestorOrSelf<Canvas>(this) is Canvas canvas &&
                !busy)
            {
                AbsoluteConnectionPoint = TransformToAncestor(canvas).Transform(relativeConnectionPoint);

                DataObject data = new DataObject();
                data.SetData(typeof(IFlow), this);

                DragDrop.DoDragDrop(this, data, DragDropEffects.Link);
            }
        }

        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            if (e.Data.GetDataPresent(typeof(IFlow)) &&
                e.Data.GetData(typeof(IFlow)) is FlowOut srcFlowOut &&
                !busy)
            {
                Canvas canvas = WpfHelper.FindAncestorOrSelf<Canvas>(this);
                AbsoluteConnectionPoint = TransformToAncestor(canvas).Transform(relativeConnectionPoint);
                flowPath = new FlowPath(srcFlowOut.AbsoluteConnectionPoint, AbsoluteConnectionPoint);
                canvas.Children.Add(flowPath);
                srcFlowOut.AddFlowPath(flowPath);
                busy = true;
            }
        }

        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);

            if (e.Data.GetDataPresent(typeof(IFlow)) &&
                e.Data.GetData(typeof(IFlow)) is FlowOut &&
                !busy)
            {
                e.Effects = DragDropEffects.Link;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        internal void AddFlowPath(FlowPath flowPath)
        {
            this.flowPath = flowPath;
            busy = true;
        }
    }
}
