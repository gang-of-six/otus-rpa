﻿using System.Windows;
using System.Windows.Controls;

namespace OtusRPA.UI
{
    public interface IFlow
    {
        public Point AbsoluteConnectionPoint { get; set; }
    }
}