﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Diagnostics;
using System.Windows.Input;
using System.Windows.Media;
using OtusRPA.Activities;

namespace OtusRPA.UI
{
    /// <summary>
    /// Interaction logic for Rectangle.xaml
    /// </summary>
    public partial class Rectangle : UserControl
    {
        public Point? DragPoint { get; private set; } = null;
        public Activity Activity { get; set; }

        public Rectangle(Activity activity)
        {
            InitializeComponent();
            Activity = activity;
            DrawThis();
        }

        public Rectangle(Rectangle c)
        {
            InitializeComponent();
            Height = c.Height;
            Width = c.Width;
            rectUI.Fill = c.rectUI.Fill;
            Activity = c.Activity;
            DrawThis();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property.Name == "Left")
            {
                double oldValue = double.IsNaN((double)e.OldValue) ? 0 : (double)e.OldValue;
                foreach (IFlow child in flowsIn.Children)
                {
                    child.AbsoluteConnectionPoint = new Point(child.AbsoluteConnectionPoint.X + (double)e.NewValue - oldValue, child.AbsoluteConnectionPoint.Y);
                }
                foreach (IFlow child in flowsOut.Children)
                {
                    child.AbsoluteConnectionPoint = new Point(child.AbsoluteConnectionPoint.X + (double)e.NewValue - oldValue, child.AbsoluteConnectionPoint.Y);
                }
            }
            else if (e.Property.Name == "Top")
            {
                double oldValue = double.IsNaN((double)e.OldValue) ? 0 : (double)e.OldValue;
                foreach (IFlow child in flowsIn.Children)
                {
                    child.AbsoluteConnectionPoint = new Point(child.AbsoluteConnectionPoint.X, child.AbsoluteConnectionPoint.Y + (double)e.NewValue - oldValue);
                }
                foreach (IFlow child in flowsOut.Children)
                {
                    child.AbsoluteConnectionPoint = new Point(child.AbsoluteConnectionPoint.X, child.AbsoluteConnectionPoint.Y + (double)e.NewValue - oldValue);
                }
            }
        }

        private void DrawThis()
        {
            int counter = 1;
            foreach (var fIn in Activity.InTypes)
            {
                flowsIn.Children.Add(new FlowIn(counter));
                counter++;
            }

            counter = 1;
            foreach (var fOut in Activity.OutTypes)
            {
                flowsOut.Children.Add(new FlowOut(counter));
                counter++;
            }

            flowLabel.Content = string.IsNullOrEmpty(Activity.Name) ? Activity.GetType().Name : Activity.Name;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            if (e.ChangedButton == MouseButton.Left)
            {
                DragPoint = e.GetPosition(this);
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.ChangedButton == MouseButton.Left)
            {
                DragPoint = null;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed &&
                DragPoint.HasValue)
            {
                DataObject data = new DataObject();
                data.SetData(typeof(Rectangle), this);

                if (WpfHelper.FindAncestorOrSelf<StackPanel>(this) is StackPanel)
                    DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);
                else
                    DragDrop.DoDragDrop(this, data, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }
    }
}
