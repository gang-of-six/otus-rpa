﻿namespace ConfigManager
{
    public class RabbitMQConfig
    {
        public string HostName { get; set; } = null;
        public string UserName { get; set; } = null;
        public string Password { get; set; } = null;
    }
}
