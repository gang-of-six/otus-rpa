# **Инструкция**
Заходим в папку с данным проектом в командной строке/PS/терминале и выполняем следующие команды:
```
dotnet user-secrets init
dotnet user-secrets set "RabbitMQConfig:HostName" "IP-адрес"
dotnet user-secrets set "RabbitMQConfig:UserName" "имя_пользователя"
dotnet user-secrets set "RabbitMQConfig:Password" "пароль"
```
Для получения доступа к сохранённым настройкам/паролям подключаем проект в зависимости и:
```
using ConfigManager;

void Foo()
{
    Console.WriteLine(ConfigReader.RabbitMQ.Password);
}
```
