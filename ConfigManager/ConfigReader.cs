﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace ConfigManager
{
    public static class ConfigReader
    {
        public static RabbitMQConfig RabbitMQ { get; private set; } = new RabbitMQConfig();
        public static IConfigurationRoot Configuration { get; set; }

        static ConfigReader()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddUserSecrets<RabbitMQConfig>()
                .Build();

            RabbitMQ = Configuration.GetSection(nameof(RabbitMQConfig))
                                    .Get<RabbitMQConfig>();
        }
    }
}
